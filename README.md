# Chat App Laravel Vue

### Installation

- Clone the repository.
- Create the database and add to .env file.
- Run Command as below
    - composer install
    - npm install
    - php artisan migarte --seed
    - php artisan serve
    - npm run dev
- Register and chat.

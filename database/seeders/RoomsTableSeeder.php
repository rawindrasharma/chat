<?php

namespace Database\Seeders;

use App\Models\ChatRoom;
use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ChatRoom::create([
            'name' => 'General',
        ]);

        ChatRoom::create([
            'name' => 'Anime',
        ]);

        ChatRoom::create([
            'name' => 'Game',
        ]);

        ChatRoom::create([
            'name' => 'Movie',
        ]);

        ChatRoom::create([
            'name' => 'Tech',
        ]);
    }
}

<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'John Doe',
            'email' => 'johndoe@gmail.com',
            'password' => Hash::make('password')
        ]);

        $user->rooms()->sync([1]);

        $user1 = User::create([
            'name' => 'Jane Doe',
            'email' => 'janedoe@gmail.com',
            'password' => Hash::make('password')
        ]);
        $user1->rooms()->sync([2]);

        $user2 = User::create([
            'name' => 'Baby Doe',
            'email' => 'babydoe@gmail.com',
            'password' => Hash::make('password')
        ]);
        $user2->rooms()->sync([1, 2]);
    }
}

<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\PrivateChatController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')
    ->group(function () {

        /**
         * User route
         */
        Route::prefix('user')
            ->group(function () {
                Route::get('/', [AuthController::class, 'user']);
            });

        Route::get('/room/{id}/users', [ChatController::class, 'getAllUsersByRoom']);
        Route::get('/room/{roomId}/messages', [ChatController::class, 'getAllMessages']);
        Route::post('/room/{roomId}/message', [ChatController::class, 'createMessage']);
        Route::get('/user/rooms', [ChatController::class, 'getAllRoomsByUser']);
        Route::get('/rooms', [ChatController::class, 'getAllRooms']);
        Route::post('/user/rooms', [ChatController::class, 'createUserRooms']);

        Route::get('/private', [PrivateChatController::class, 'private']);
        Route::get('/private/users', [PrivateChatController::class, 'privateUsers']);
        Route::get('/private/messages/{receiverId}', [PrivateChatController::class, 'privateMessages']);
        Route::post('/private/{receiverId}/message', [PrivateChatController::class, 'createPrivateMessage']);
    });

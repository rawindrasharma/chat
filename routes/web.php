<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::post('logout', [AuthController::class, 'logout']);

/**
 * Redirect all route except above to 'app' view
 */
Route::view('{any}', 'app')->where('any', '.*');

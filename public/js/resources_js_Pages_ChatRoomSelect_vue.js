(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_ChatRoomSelect_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/ChatRoomSelect.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/ChatRoomSelect.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  data: function data() {
    return {
      rooms: [],
      selectedRooms: []
    };
  },
  methods: {
    getAllRooms: function getAllRooms() {
      var _this = this;

      axios.get('/api/rooms').then(function (res) {
        _this.rooms = res.data;

        _this.getChatRoom();
      })["catch"](function (error) {
        return console.log(error);
      });
    },
    getChatRoom: function getChatRoom() {
      var _this2 = this;

      axios.get('/api/user/rooms').then(function (res) {
        _this2.selectedRooms = res.data.rooms.map(function (el) {
          return el.id;
        });
      })["catch"](function (errors) {
        _this2.error = errors.response.data;
      });
    },
    chat: function chat() {
      var _this3 = this;

      axios.post('/api/user/rooms', this.selectedRooms).then(function () {
        _this3.$router.push({
          name: 'chat'
        });
      })["catch"](function (errors) {
        _this3.error = errors.response.data;
      });
    }
  },
  created: function created() {
    this.getAllRooms();
  }
});

/***/ }),

/***/ "./resources/js/Pages/ChatRoomSelect.vue":
/*!***********************************************!*\
  !*** ./resources/js/Pages/ChatRoomSelect.vue ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _ChatRoomSelect_vue_vue_type_template_id_437ff998___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ChatRoomSelect.vue?vue&type=template&id=437ff998& */ "./resources/js/Pages/ChatRoomSelect.vue?vue&type=template&id=437ff998&");
/* harmony import */ var _ChatRoomSelect_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ChatRoomSelect.vue?vue&type=script&lang=js& */ "./resources/js/Pages/ChatRoomSelect.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _ChatRoomSelect_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _ChatRoomSelect_vue_vue_type_template_id_437ff998___WEBPACK_IMPORTED_MODULE_0__.render,
  _ChatRoomSelect_vue_vue_type_template_id_437ff998___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/ChatRoomSelect.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/ChatRoomSelect.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/Pages/ChatRoomSelect.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ChatRoomSelect_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ChatRoomSelect.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/ChatRoomSelect.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ChatRoomSelect_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/ChatRoomSelect.vue?vue&type=template&id=437ff998&":
/*!******************************************************************************!*\
  !*** ./resources/js/Pages/ChatRoomSelect.vue?vue&type=template&id=437ff998& ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ChatRoomSelect_vue_vue_type_template_id_437ff998___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ChatRoomSelect_vue_vue_type_template_id_437ff998___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ChatRoomSelect_vue_vue_type_template_id_437ff998___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ChatRoomSelect.vue?vue&type=template&id=437ff998& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/ChatRoomSelect.vue?vue&type=template&id=437ff998&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/ChatRoomSelect.vue?vue&type=template&id=437ff998&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/ChatRoomSelect.vue?vue&type=template&id=437ff998& ***!
  \*********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass:
        "flex items-center justify-center h-screen mx-auto bg-gray-100"
    },
    [
      _c(
        "div",
        {
          staticClass:
            "flex flex-col px-6 py-3 bg-white rounded-md shadow-xl w-7xl"
        },
        [
          _c(
            "h1",
            { staticClass: "mt-3 mb-6 text-xl font-bold text-indigo-600" },
            [_vm._v("Select At Least One Room")]
          ),
          _vm._v(" "),
          _vm._l(_vm.rooms, function(room, index) {
            return _c("div", { key: index, staticClass: "form-check" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.selectedRooms,
                    expression: "selectedRooms"
                  }
                ],
                staticClass: "form-check-input",
                attrs: { type: "checkbox" },
                domProps: {
                  value: room.id,
                  checked: Array.isArray(_vm.selectedRooms)
                    ? _vm._i(_vm.selectedRooms, room.id) > -1
                    : _vm.selectedRooms
                },
                on: {
                  change: function($event) {
                    var $$a = _vm.selectedRooms,
                      $$el = $event.target,
                      $$c = $$el.checked ? true : false
                    if (Array.isArray($$a)) {
                      var $$v = room.id,
                        $$i = _vm._i($$a, $$v)
                      if ($$el.checked) {
                        $$i < 0 && (_vm.selectedRooms = $$a.concat([$$v]))
                      } else {
                        $$i > -1 &&
                          (_vm.selectedRooms = $$a
                            .slice(0, $$i)
                            .concat($$a.slice($$i + 1)))
                      }
                    } else {
                      _vm.selectedRooms = $$c
                    }
                  }
                }
              }),
              _vm._v(" "),
              _c("label", { staticClass: "form-check-label" }, [
                _vm._v(_vm._s(room.name))
              ])
            ])
          }),
          _vm._v(" "),
          _c("div", { staticClass: "flex justify-center" }, [
            _c(
              "button",
              {
                staticClass:
                  "w-48 px-8 py-3 mt-6 font-semibold text-white bg-indigo-700 rounded-md hover:bg-indigo-500 text-md",
                on: { click: _vm.chat }
              },
              [_vm._v("Chat")]
            )
          ])
        ],
        2
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_PrivateChat_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PrivateChat.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PrivateChat.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    Message: function Message() {
      return __webpack_require__.e(/*! import() */ "resources_js_Components_Message_vue").then(__webpack_require__.bind(__webpack_require__, /*! ../Components/Message */ "./resources/js/Components/Message.vue"));
    },
    PrivateMessage: function PrivateMessage() {
      return __webpack_require__.e(/*! import() */ "resources_js_Components_PrivateMessage_vue").then(__webpack_require__.bind(__webpack_require__, /*! ../Components/PrivateMessage */ "./resources/js/Components/PrivateMessage.vue"));
    }
  },
  data: function data() {
    return {
      users: [],
      currentUser: {},
      messages: []
    };
  },
  watch: {
    currentUser: function currentUser(oldUser) {
      if (oldUser.id) {
        console.log("disconnect");
        this.disconnect(oldUser);
      }

      this.connect();
    }
  },
  methods: {
    connect: function connect() {
      if (this.currentUser.id) {
        console.log("connect");
        var vm = this;
        this.getPrivateMessages();
        window.Echo["private"]("privatechat." + this.$store.state.auth.user.id).listen(".privatemessage.new", function (e) {
          return vm.getPrivateMessages();
        });
      }
    },
    disconnect: function disconnect(user) {
      window.Echo.leave("privatechat." + user.id);
    },
    getAllPrivateUsers: function getAllPrivateUsers() {
      var _this = this;

      axios.get("/api/private/users").then(function (res) {
        _this.users = res.data;

        _this.defaultUser(res.data[0]);
      })["catch"](function (err) {
        return console.log(err);
      });
    },
    getPrivateMessages: function getPrivateMessages() {
      var _this2 = this;

      axios.get("/api/private/messages/".concat(this.currentUser.id)).then(function (res) {
        return _this2.messages = res.data;
      })["catch"](function (err) {
        return console.log(err);
      });
    },
    defaultUser: function defaultUser(user) {
      this.currentUser = user;
    }
  },
  created: function created() {
    this.getAllPrivateUsers();
  }
});

/***/ }),

/***/ "./resources/js/Pages/PrivateChat.vue":
/*!********************************************!*\
  !*** ./resources/js/Pages/PrivateChat.vue ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _PrivateChat_vue_vue_type_template_id_f945523c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PrivateChat.vue?vue&type=template&id=f945523c& */ "./resources/js/Pages/PrivateChat.vue?vue&type=template&id=f945523c&");
/* harmony import */ var _PrivateChat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PrivateChat.vue?vue&type=script&lang=js& */ "./resources/js/Pages/PrivateChat.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _PrivateChat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _PrivateChat_vue_vue_type_template_id_f945523c___WEBPACK_IMPORTED_MODULE_0__.render,
  _PrivateChat_vue_vue_type_template_id_f945523c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/PrivateChat.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/PrivateChat.vue?vue&type=script&lang=js&":
/*!*********************************************************************!*\
  !*** ./resources/js/Pages/PrivateChat.vue?vue&type=script&lang=js& ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PrivateChat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./PrivateChat.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PrivateChat.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PrivateChat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/PrivateChat.vue?vue&type=template&id=f945523c&":
/*!***************************************************************************!*\
  !*** ./resources/js/Pages/PrivateChat.vue?vue&type=template&id=f945523c& ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PrivateChat_vue_vue_type_template_id_f945523c___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PrivateChat_vue_vue_type_template_id_f945523c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PrivateChat_vue_vue_type_template_id_f945523c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./PrivateChat.vue?vue&type=template&id=f945523c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PrivateChat.vue?vue&type=template&id=f945523c&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PrivateChat.vue?vue&type=template&id=f945523c&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PrivateChat.vue?vue&type=template&id=f945523c& ***!
  \******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "messaging" }, [
      _c("div", { staticClass: "inbox_msg" }, [
        _c("div", { staticClass: "inbox_people" }, [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "inbox_chat" },
            _vm._l(_vm.users, function(user, index) {
              return _c(
                "div",
                { key: index, staticClass: "chat_list active_chat" },
                [
                  _c(
                    "div",
                    {
                      staticClass: "chat_people",
                      on: {
                        click: function($event) {
                          return _vm.defaultUser(user)
                        }
                      }
                    },
                    [
                      _vm._m(1, true),
                      _vm._v(" "),
                      _c("div", { staticClass: "chat_ib" }, [
                        _c("h5", [
                          _vm._v("\n                  " + _vm._s(user.name)),
                          _c("span", { staticClass: "chat_date" }, [
                            _vm._v("last chat dateDec 25")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("p", [_vm._v("last chat message")])
                      ])
                    ]
                  )
                ]
              )
            }),
            0
          )
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "mesgs" },
          [
            _c("Message", { attrs: { messages: _vm.messages } }),
            _vm._v(" "),
            _c("PrivateMessage", {
              attrs: { user: _vm.currentUser },
              on: { messageSent: _vm.getPrivateMessages }
            })
          ],
          1
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "headind_srch" }, [
      _c("div", { staticClass: "recent_heading" }, [
        _c("h4", [_vm._v("Friends List")])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "srch_bar" }, [
        _c("div", { staticClass: "stylish-input-group" }, [
          _c("input", {
            staticClass: "search-bar",
            attrs: { type: "text", placeholder: "Search" }
          }),
          _vm._v(" "),
          _c("span", { staticClass: "input-group-addon" }, [
            _c("button", { attrs: { type: "button" } }, [
              _c("i", {
                staticClass: "fa fa-search",
                attrs: { "aria-hidden": "true" }
              })
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "chat_img" }, [
      _c("img", {
        attrs: {
          src: "https://ptetutorials.com/images/user-profile.png",
          alt: "sunil"
        }
      })
    ])
  }
]
render._withStripped = true



/***/ })

}]);
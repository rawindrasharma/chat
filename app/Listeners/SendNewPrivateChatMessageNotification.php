<?php

namespace App\Listeners;

use App\Events\NewPrivateChatMessage;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendNewPrivateChatMessageNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewPrivateChatMessage  $event
     * @return void
     */
    public function handle(NewPrivateChatMessage $event)
    {
        //
    }
}

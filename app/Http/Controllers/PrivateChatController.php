<?php

namespace App\Http\Controllers;

use App\Events\NewPrivateChatMessage;
use App\Models\PrivateChat;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Image;
use App\Traits\ImageUploadTrait;
use Illuminate\Http\UploadedFile;

class PrivateChatController extends Controller
{
    use ImageUploadTrait;

    public function privateUsers()
    {
        return User::all()->except(Auth::id());
    }

    public function privateMessages($receiverId)
    {
        $message = PrivateChat::with('user', 'image')
            ->where(['user_id' => auth()->id(), 'receiver_id' => $receiverId])

            // ->orWhere(['user_id' => $receiverId, 'receiver_id' => auth()->id()])
            ->orWhere(
                function ($query) use ($receiverId) {

                    $query->where(['user_id' => $receiverId, 'receiver_id' => auth()->id()]);
                }
            )
            ->get();

        return $message;
    }

    public function createPrivateMessage(Request $request, $receiverId)
    {

        $privateMessage = $request->except('_token', 'file');
        $privateMessage['user_id'] = Auth::id();
        $privateMessage['receiver_id'] = $receiverId;

        $privateMessage = PrivateChat::create($privateMessage);
        if ($request->file) {
            $path = $this->uploadImage($request->file, "images/");
            $image = new Image(['path' => $path]);
            $privateMessage->image()->save($image);
        }
        broadcast(new NewPrivateChatMessage($privateMessage))->toOthers();

        return $privateMessage;
    }
}

<?php

namespace App\Http\Controllers;

use App\Events\NewChatMessage;
use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function getAllUsersByRoom($id)
    {
        return User::whereHas('rooms', function ($query) use ($id) {
            $query->where('chat_room_id', $id);
        })->get();
    }

    public function getAllMessages($roomId)
    {
        $chatMessages = ChatMessage::where('chat_room_id', $roomId)->with('user')
            ->orderBy('created_at', 'ASC')->get();


        return $chatMessages;
    }

    public function createMessage(Request $request, $roomId)
    {
        $message = $request->except('_token');
        $message['user_id'] = Auth::id();
        $message['chat_room_id'] = $roomId;

        $message = ChatMessage::create($message);
        broadcast(new NewChatMessage($message))->toOthers();

        return $message;
    }

    public function getAllRoomsByUser()
    {
        return User::with('rooms')->where('id', auth()->id())->first();
    }

    public function getAllRooms()
    {
        return ChatRoom::all();
    }

    public function createUserRooms(Request $request)
    {
        return Auth::user()->rooms()->sync($request->all());
        // $user2->rooms()->sync([1, 2]);
    }
}

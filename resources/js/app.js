require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import router from './Router'
import store from './store/index';



Vue.use(VueRouter)

store.dispatch('auth/getUser').then(() => {
    new Vue({
        el: '#app',
        router,
        store,
        components: { App }
    })
})

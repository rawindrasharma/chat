import axios from 'axios'

export default {
    namespaced: true,

    state: {
        authenticated: false,
        user: null
    },

    getters: {
        authenticated(state) {
            return state.authenticated
        },

        user(state) {
            return state.user
        },
    },

    mutations: {
        SET_AUTHENTICATED(state, value) {
            state.authenticated = value
        },

        SET_USER(state, value) {
            state.user = value
        }
    },

    actions: {
        async login({ dispatch }, credentials) {

            return new Promise((resolve, reject) => {
                axios.get('/sanctum/csrf-cookie');

                axios.post('/login', credentials)
                    .then(res => {
                        dispatch('getUser');
                        resolve(res);
                    })
                    .catch(errors => {
                        reject(errors)
                    });
            })
        },

        async logout({ dispatch }) {
            await axios.post('/logout');

            return dispatch('getUser')
        },

        getUser({ commit }) {
            return axios.get('/api/user').then((response) => {
                commit('SET_AUTHENTICATED', true);
                commit('SET_USER', response.data)
            }).catch(() => {
                commit('SET_AUTHENTICATED', false);
                commit('SET_USER', null)
            })
        }
    }
}

const routes = [
    {
        path: '',
        component: () => import('../Pages/Login'),
        name: 'login'
    },
    {
        path: '/register',
        component: () => import('../Pages/Register'),
        name: 'register'
    },
    {
        path: '/chat',
        component: () => import('../Pages/Chat'),
        name: 'chat'
    },
    {
        path: '/private-chat',
        component: () => import('../Pages/PrivateChat'),
        name: 'private-chat'
    },
    {
        path: '/select-room',
        component: () => import('../Pages/ChatRoomSelect'),
        name: 'select-room'
    }
]

export default routes